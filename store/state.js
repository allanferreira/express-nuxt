import blog from './blog/state'
import user from './user/state'

export default { ...blog, ...user }
import blog from './blog/mutations'
import user from './user/mutations'

export default { ...blog, ...user }
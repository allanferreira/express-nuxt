import blog from './blog/actions'
import user from './user/actions'

export default { ...blog, ...user }
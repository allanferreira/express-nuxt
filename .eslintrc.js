module.exports = {
    root: true,
    parser: 'babel-eslint',
    env: {
        browser: true,
        node: true
    },
    extends: 'standard',
    plugins: [
        'html'
    ],
    rules: {
        "space-before-function-paren": 0,
        "indent": 0,
        "no-tabs": 0,
        "eol-last": 0,
        "space-in-parens": 0
    },
    globals: {}
}
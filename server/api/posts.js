import { Router } from 'express'

const router = Router()
const posts = [
  { 
    name: 'Allan Ferreira',
    slug: 'allan-ferreira'
  }
]

const single = {
  "allan-ferreira": {
    name: "Allan Ferreira"
  }
}

router.get('/posts', (req, res, next) => {
  res.json(posts)
})

router.get('/posts/:slug', (req, res, next) => {

  const slug = req.params.slug
  single[slug] ? res.json(single[slug]) : res.sendStatus(404)

})

export default router
